<?php

  final class SiteAttention {

    const VERSION = '1.1.0';

    const URL = 'https://api.siteattention.com/';

    const KEY = 'SA_KEY';

    const IID = 'SA_IID';

    const INAME = 'SA_INAME';

    const ILOCKED = 'SA_ILOCKED';

    const GUID = 'SA_GUID';

    // send the plugin notification to API
    public static function sendPluginEvent($type) {
      return self::makeApiCall(
        self::URL . 'record',
        [
          'meta' => [
            'timestamp' => date('Y-m-d H:i:s'),
            'event' => 'plugin',
            'type' => $type,
            'key' => variable_get(SiteAttention::KEY),
            'instance' => variable_get(SiteAttention::IID),
            'guid' => variable_get(SiteAttention::GUID),
            'browser' => 'Drupal/PHP' . phpversion(),
            'cms' => 'Drupal',
            'domain' => $GLOBALS['base_root'],
          ],
          'data' => [
            'cmsVersion' => self::getDrupalVersion(),
            'saVersion' => self::VERSION,
          ],
        ]
      );
    }

    // announce API of page change
    public static function publish($node) {
      global $language_content;

      $key = variable_get(SiteAttention::KEY);

      if (!$key) {
        return;
      }

      self::makeApiCall(
        self::URL . 'customer/instance/page/publish',
        [
          'published' => $node->status,
          'url' => self::getNodeUrl($node),
          'pid' => $node->nid,
          'iid' => variable_get(SiteAttention::IID, ''),
          'lang' => $language_content->language,
        ],
        $key
      );
    }

    // get the full url of a node
    public static function getNodeUrl($node) {
      if (!isset ($node->nid)) {
        return NULL;
      }

      return url(
        drupal_get_path_alias('node/' . $node->nid),
        [
          'absolute' => TRUE,
        ]
      );
    }

    // makes a call to API, setting all the needed parameters
    private static function makeApiCall($url, $data, $key = "") {
      if (!is_array($data)) {
        return;
      }

      $response = drupal_http_request(
        $url,
        [
          'headers' => [
            'X-SiteAttention' => $key,
            'Content-Type' => 'application/json',
            'Referer' => $GLOBALS['base_url'],
          ],
          'method' => 'POST',
          'data' => json_encode($data),
          'timeout' => 15,
        ]
      );

      if ($response->code != 200) {
        $status_message = isset($response->status_message) ? $response->status_message : '';
        $status_message = isset($status_message) ?: $status_message . "\n";

        return [
          'success' => 'false',
          'data' => NULL,
          'error' => $status_message . $response->error,
        ];
      }

      return $response->data;
    }

    // returns the Drupal version
    private static function getDrupalVersion() {
      // Drupal 7.x
      if (defined('VERSION')) {
        return VERSION;
      }

      // old stuff
      return "<4.7.2";
    }
  }
