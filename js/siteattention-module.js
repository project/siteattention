/** version 7x1.1
 * @file
 * Script for the SiteAttention plugin.
 */

var SiteAttentionDrupal = function ( data )
{
    /**
     * The implementation of the FieldAbstract class specific for Drupal 7 image
     * field. This acts as a wrapper and listener for the `Image Add Widget`.
     * This class inherits FieldAbstract and uses the same parameters for it's
     * constructor.
     *
     * @class      FieldDrupal7Image Drupal 7 image field type
     */
     var FieldDrupal7Image = function () {

        function FieldDrupal7Image(field){

            const local = SiteAttentionModule.FieldAbstract.call(this,field);

            local.load = function ()
            {
                if (!local.this._find()) return false;

                if (!local.element.getElementsByClassName('image-widget-data'))
                {
                    console.warn('SiteAttention: Field <' +local.name+ '> <' +local.selector[0]+ '> could not be resolved to FieldDrupal7Image.');
                }

                local.object = new parent.MutationObserver(local.listener);

                var element = document.querySelector(local.selector);


                var imageFieldInputs = element.querySelectorAll('input');

                imageFieldInputs.forEach(function(el){
                  if(el.id.includes('edit') && el.id.includes('alt') && el.type !== 'hidden' && !el.id.includes('button') && el.id.includes('image') && !el.id.includes('upload')){
                    local.alt = el;
                    local.alt.addEventListener('keyup',local.listener);
                    local.container = el;
                  }
                })

                if(!local.alt){
                  return false;
                }

                local.listener();
                local.bind();
                local.update();

                return true;
            }

            local.bind = function ()
            {

                local.object.observe(local.element,{
                    childList: true,
                    attributes: false,
                    characterData: true,
                    subtree: true,
                    attributeOldValue: false,
                    characterDataOldValue: false,
                });
            }

            local.listener = function (mutations,observer)
            {
                if (observer instanceof parent.MutationObserver) // overlay issues so use parent
                {
                    const mutation = mutations[0];

                    if
                    (
                        mutation.type === 'childList' &&
                        mutation.removedNodes.length &&
                        mutation.removedNodes[0].className === 'ajax-progress ajax-progress-throbber'
                    )
                    {
                        local.bind();
                        //local.load();
                        local.update();
                        local.this._update_timer.call(local.this);
                    }
                }

                else
                {
                    local.this._update_timer.call(local.this);
                }
            }

            local.update = function ()
            {
                const imgs = local.element.querySelectorAll('img');

                imgs.forEach(function(img, index){

                  if(img.type !== 'hidden'){

                    const imgClone = img.cloneNode();
                    imgClone.attributes.alt.value = local.alt.value;
                    let imgsrc = img.nextElementSibling.href;
                    if(imgsrc){
                      imgClone.attributes.src.value = imgsrc;
                    }

                    imgClone.style.width = '150px';
                    imgClone.style.height = '150px';
                    local.text = imgClone.attributes.alt.value;
                    local.html = local.this._nodeToHtml(imgClone);
                  }

                })

            }

            local.unload = function ()
            {
                local.unbind();
            }

            local.unbind = function ()
            {
                local.object.disconnect();
            }

            local.focus = function ()
            {

                if(local.container){
                  local.container.dispatchEvent( local.this._focus_event );
                  local.container.focus();
                }

            }
        };

        FieldDrupal7Image.prototype = SiteAttentionModule.FieldAbstract.prototype;

        FieldDrupal7Image.prototype.constructor = FieldDrupal7Image;

        return FieldDrupal7Image;
    };
    /**********************End of FieldDrupal7Image function******************/

    var local =
    {
        document : undefined,
        resize : undefined,
        click: undefined,
        container : undefined,
        header : undefined,
        overlay : undefined,
        content : undefined,
        adminMenu : undefined,
        published : undefined,
        url: undefined,
        urlTab: undefined,
        summary: undefined,
        summaryTab: undefined,
        summaryToggle: undefined
    }

    var getPid = function ()
    {
        return Drupal.settings.SiteAttention.nodeId;
    }

    var getIid = function ()
    {
        return Drupal.settings.SiteAttention.iid;
    }

    var getType = function ()
    {
        return Drupal.settings.SiteAttention.nodeType;
    }

    var getLang = function ()
    {
        return Drupal.settings.SiteAttention.nodeLang;
    }

    var getUserName = function ()
    {
        return Drupal.settings.SiteAttention.userName;
    }

    var getUserId = function ()
    {
        return Drupal.settings.SiteAttention.userId;
    }

    var getUrl = function ()
    {
        return Drupal.settings.SiteAttention.nodeUrl;
    }

    var getPublished = function ()
    {
        return Drupal.settings.SiteAttention.nodeStatus;
    }

    /**************************************************************************
    *Observe for DOM change inside the HTML element that wraps all the fields
    ***************************************************************************/

    // Select the node(that wraps the page on editor) that will be observed for mutations
    var pageWrapper = document.getElementById('sa-node-edit');
    var fieldsetWrapper = document.getElementById('edit-field-image');

    var fieldsetConfig = {childList: true,
                  attributes: false,
                  characterData: true,
                  subtree: true,
                  attributeOldValue: false,
                  characterDataOldValue: false};


    var pageConfig = {childList: true,
                  attributes: false,
                  characterData: true,
                  subtree: true,
                  attributeOldValue: false,
                  characterDataOldValue: false};
    var callback = function(mutationsList) {

        for(var mutation of mutationsList) {

            if (mutation.type == 'childList') {
              if (mutation.addedNodes.length >= 1) {
                  load();
                }
                else if (mutation.removedNodes.length >= 1) {
                  load();
                }
            }

        }

    };
    var observer = new MutationObserver(callback);

    if(pageWrapper){
      observer.observe(pageWrapper, pageConfig);
    }

    if(fieldsetWrapper){
      observer.observe(fieldsetWrapper, fieldsetConfig);
    }

    var mutationsList = observer.takeRecords();





    /**********************End of Observer function*****************************/

    /**************************************************************************
    * Alert messages from SiteAttention using 'alertbox'
    ***************************************************************************/
    Element.prototype.remove = function() {
    this.parentElement.removeChild(this);
    }
    NodeList.prototype.remove = HTMLCollection.prototype.remove = function() {
        for(var i = this.length - 1; i >= 0; i--) {
            if(this[i] && this[i].parentElement) {
                this[i].parentElement.removeChild(this[i]);
            }
        }
    }
    function insertAfter(newNode, referenceNode) {
          referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
      }

    /**Any alert messages to be shown**/
    var alertbox = function(message, element){

      let box = document.createElement('div');
      box.style.border = '1px solid rgb(77, 176, 233)';
      box.style.backgroundColor = '#edf5fa';
      box.style.padding = '10px';
      box.id = 'SiteAttention_alert_box';

      let content = document.createTextNode(message);

      box.appendChild(content);
      insertAfter(box, element);

      /*setTimeout(function(){
        document.getElementById('SiteAttention_alert_box').remove();
      }, 10000);*/
    }
    /***************End of alertbox function************************/



    /**************************************************************************
    * Get Fields and Get Labels for Load function
    ***************************************************************************/
    var getFieldLabel = function(attribute){
      var fieldmaps = Drupal.settings.SiteAttention.fieldmaps;
      var label;
      Object.keys(fieldmaps).map(e =>{
        //console.log(e, fieldmaps[e]);
        if(attribute.includes(e) || e.includes(attribute)){
          label = fieldmaps[e];
        }
      })

      if(label){
        return label;
      } else{
        return 'Field Unknown';
      }
    }

    var getAllFields = function(){

      var fields = [];
      var textfieldArray = [];
      var imagefieldArray=[];

      var array = document.querySelectorAll('.sa-field input, .sa-field textarea, textarea.sa-field, input.sa-field, #edit-path-alias');
      if(array){
        array.forEach(function(el){
          if(el.id.includes('edit') && el.type !== 'hidden' && !el.id.includes('button') && !el.id.includes('image') && el.type !== 'submit'){
            textfieldArray.push(el);
          }
        })
      }

      if(textfieldArray){
        textfieldArray.forEach(function(el, index) {

          if(el.disabled == true){
            el.disabled = false;
          }else if(el.hidden == true){
            el.hidden = false;
          }

            var isTitle = el.id.includes('title');

            var isSummary = el.id.includes('summary');

            var isUrl= el.id.includes('path-alias');

            var isBody = el.id.includes('body');

            if(el.id !== "" ){
              var selector = "#" + el.id;
            } else{
              var selector = '.'+ el.className;
            }

            var innerText = getFieldLabel(el.name);

            if(isTitle){
              if(typeof el.labels !== 'undefined'){
                innerText = el.labels[0].innerText;
              } else{
                innerText = 'Title';
              }
              fields.push(
              {
                seo: 'title',
                name:innerText+'__'+index,
                selector: selector
              },
              {
                seo: 'content',
                name: innerText+'__'+index,
                selector: selector,
                tag: 'h1'
              }
            );
          }else if(isSummary){

              if(!document.getElementById('SiteAttention_alert_box'))
              {
                let message = "SiteAttention: SEO Tool requires the Body Summary to remain visible. Therefore it is recommended not to Hide Summary";
                let element = document.querySelector(selector);
                alertbox(message,element);
              }

              if(typeof el.labels !== 'undefined'){
                innerText = el.labels[0].innerText;
                innerText = innerText.replace(/ *\([^)]*\) */g, ""); //remove brackets
              } else{
                innerText = 'Summary';
              }

              fields.push(
              {
                  seo: 'metadescription',
                  name: innerText+'__'+index, //label of the field
                  selector: selector,

              });
            }else if(isUrl){
              fields.push(
                {
                  seo: 'url',
                  name: 'URL alias',
                  selector: selector
                }
              )
            } else if(isBody && !isSummary){
              fields.push(
                {
                  seo:'content',
                  name:'Body'+'__'+index,
                  selector:selector,
                }
              )
            }else{
              fields.push(
                {
                  seo:'content',
                  name: innerText+'__'+index,
                  selector:selector
                }
              )
            }

        })
      }

      var imagearray = document.querySelectorAll('.sa-field .image-widget-data');

      if(imagearray){
        imagearray.forEach(function(el, index){

            if( !el.innerHTML.includes('upload')){
              el.id = 'sa_image_'+index;
              imagefieldArray.push(el);
            }

        })

        if(imagefieldArray){
          imagefieldArray.forEach(function(el, index){

              if(el.id !== "" ){
                var selector = "#" + el.id;
              } else{
                var selector = '.'+ el.className;
              }

              let innerText;
              if(typeof el.labels !== 'undefined'){
                innerText = el.labels[0].innerText;
              } else{
                innerText = 'Alternative text';
              }

              fields.push(
              {
                  seo: 'content',
                  name: innerText+'__'+index,
                  selector: selector,
                  type:FieldDrupal7Image
              });

            });
        }

      }
      return fields;
    }

/**********End of getFields and getFieldLabel functions***************/


    var load = function(){

        SiteAttention.load({
            cms: SiteAttentionModule.Cms.Drupal,
            pid: getPid(),
            iid: getIid(),
            type: getType(),
            lang: getLang(),
            user: getUserName(),
            url: getUrl(),
            published: getPublished(),
            map: SiteAttentionModule.FieldFactory(getAllFields())
        });


    }


    var getContainer = function ()
    {
        local.container = local.document.createElement('div');

        local.container.id = 'SAPL';

        // IE fix for now
        local.container.style.position = 'fixed';
        local.container.style.right = '0';
        local.container.style.bottom = '0';
        local.container.style.zIndex = '599';

        local.document.body.appendChild(local.container);

        return local.container;
    }

    var getMinimised = function ()
    {
        return false;
    }

    var onUpdateUi = function ()
    {
        return true;
    }

    var onMinimise = function ()
    {
        if ( local.overlay )
        {
            local.content.style.width = '';
        }

        else
        {
            if ( local.header )
            {
                local.header.classList.remove("SA_max_header");
            }

            if ( local.content )
            {
                local.content.classList.remove("SA_max_content");
            }
        }
    }

    var onMaximise = function ()
    {
        if ( local.overlay )
        {
            local.content.style.width
                = parent.innerWidth
                - SiteAttention.ui.main.frame.clientWidth
                + "px";
        }

        else
        {
            if ( local.header )
            {
                local.header.classList.add("SA_max_header");
            }

            if ( local.content )
            {
                local.content.classList.add("SA_max_content");
            }
        }
    }

    var onResize = function(local)
    {
        // is overlay and plugin is shown
        if ( local.overlay && local.content.style.width !== '' )
        {
            local.content.style.width
                = parent.innerWidth
                - SiteAttention.ui.main.frame.clientWidth
                + "px";
        }

        var bodyStyle = getComputedStyle(local.document.body);

        var height =
            local.document.defaultView.innerHeight -
            bodyStyle.paddingTop.replace('px','') - bodyStyle.marginTop.replace('px','');

        local.container.style.height = height + "px";
    }

    var onPlay = function ()
    {
        onResize(local);
        onMaximise();
    }
    var RemoveCssImport = function(styleTag) {

            if (styleTag) {
                    styleTag.parentNode.removeChild (styleTag);
            }
       }

    var instanceOf = function(obj,type)
    {
        // Function.name poly
        !function(){function t(){var t,e;return this===Function||this===Function.prototype.constructor?e="Function":this!==Function.prototype&&(t=(""+this).match(n),e=t&&t[1]),e||""}var n=/^\s*function\s+([^\(\s]*)\s*/,e=!("name"in Function.prototype&&"name"in function(){}),o="function"==typeof Object.defineProperty&&function(){var t;try{Object.defineProperty(Function.prototype,"_xyz",{get:function(){return"blah"},configurable:!0}),t="blah"===Function.prototype._xyz,delete Function.prototype._xyz}catch(n){t=!1}return t}(),r="function"==typeof Object.prototype.__defineGetter__&&function(){var t;try{Function.prototype.__defineGetter__("_abc",function(){return"foo"}),t="foo"===Function.prototype._abc,delete Function.prototype._abc}catch(n){t=!1}return t}();Function.prototype._name=t,e&&(o?Object.defineProperty(Function.prototype,"name",{get:function(){var n=t.call(this);return this!==Function.prototype&&Object.defineProperty(this,"name",{value:n,configurable:!0}),n},configurable:!0}):r&&Function.prototype.__defineGetter__("name",function(){var n=t.call(this);return this!==Function.prototype&&this.__defineGetter__("name",function(){return n}),n}))}();

        if (!(type instanceof Function)) return false;

        return Object.prototype.toString.call(obj).slice(8,-1) === type.name;
    }

    var setHtmlEnv = function ()
    {

      if(Drupal.settings.SiteAttention.cssUrl !== 'undefined') {
        switch ( Drupal.settings.ajaxPageState.theme )
        {
            case 'seven':
                parent.document.body.classList.add('siteattention-seven');
                local.header = "#branding";
                local.content = "#content";
                break;

            case 'garland':
                parent.document.body.classList.add('siteattention-garland');
                local.header = "#header";
                local.content = "#container";
                break;

            case 'bartik':
                parent.document.body.classList.add('siteattention-bartik');
                local.header = "#header";
                local.content = "#main";
                break;

            case 'stark':
                parent.document.body.classList.add('siteattention-stark');
                local.header = "#header";
                local.content = "#main-wrapper";
                break;

            case 'bootstrap':
                parent.document.body.classList.add('siteattention-bootstrap');
                local.header = "#navbar";
                local.content = ".main-container";
                break;

            case 'zen':
                parent.document.body.classList.add('siteattention-zen');
                local.header = ".header";
                local.content = ".layout-center";
                break;
        }

        // go on the parent window
        local.overlay = parent.document.querySelector ( '#overlay-container' );

        // set the document to the root window's document
        if ( local.overlay  )
        {
            local.document = parent.document;
            local.content = local.overlay;
        }else
        {
            local.document = document;
            local.content = document.querySelector( local.content );
        }

            // inject the css on the parent window where the container lays
            saInjectStyle(Drupal.settings.SiteAttention.cssUrl, undefined, local.document);

            var moduleStyleTagParent = document.getElementById("#SiteAttention_style_css");
            var alertboxElement = document.getElementById('SiteAttention_alert_box');

            // when page changes with ajax
            parent.addEventListener('popstate',function(){
                if(SiteAttention !== undefined)
                {
                  observer.disconnect();
                  if(alertboxElement){
                    alertboxElement.remove();
                  }
                  RemoveCssImport (moduleStyleTagParent);
                  SiteAttention.quit();
                  onMinimise();
                }
            });

            // IE fix which doesn;t work from other iframes anyways
             parent.addEventListener('hashchange',function(){
                if(SiteAttention !== undefined)
                {
                  observer.disconnect();
                  if(alertboxElement){
                    alertboxElement.remove();
                  }
                  RemoveCssImport (moduleStyleTagParent);
                  SiteAttention.quit();
                  onMinimise();
                }
            });




        local.selectBodyFormat = document.getElementById('sa-node-edit'); // select the body to observe the changes
        local.published = document.querySelector ( '#edit-status' );
        local.header = document.querySelector( local.header );
        local.url = document.querySelector ( '#edit-path-alias' );

        local.toolbar = local.document.querySelector ( '#toolbar' );
        local.adminMenu = local.document.querySelector ( '#admin-menu' );

        if (local.toolbar)
        {
            local.shortcut = local.toolbar.getElementsByClassName('toolbar-toggle-processed');
        }

        local.resize = document.createEvent('HTMLEvents');
        local.resize.initEvent('resize', true, true);
        local.resize.eventName = 'SiteAttention resize';

        local.click = document.createEvent('HTMLEvents');
        local.click.initEvent('click', true, true);
        local.click.eventName = 'SiteAttention click';

        local.focus = document.createEvent('HTMLEvents');
        local.focus.initEvent('focus', true, true);

        var tabs = Array.prototype.slice.call ( document.querySelector ( '.vertical-tabs-panes' ).children ),
            pathTab = document.querySelector ( '#edit-path' ),
            tabIndex = tabs.indexOf( pathTab );

        local.urlTab = document.querySelector ( '.vertical-tabs-list' ) .children [ tabIndex ] .children [ 0 ];

        // when focus on the URL input, trigger click so container expands
        if ( local.url && local.urlTab )
        {
          local.url.addEventListener ( 'focus' , function ( event )
          {
            local.urlTab.dispatchEvent ( local.click );
          });
        }

        local.summaryToggle = document.querySelector ( '.link-edit-summary' );
        local.summaryTab = document.querySelector ( '.text-summary-wrapper' );

        if(local.summaryToggle)
        {
          if(local.summaryTab.style.display == 'none'){
            local.summaryToggle.dispatchEvent ( local.click );
          }
        }

        if (local.shortcut)
        {
            local.shortcut[0].addEventListener('click',function(){
                onResize(local);
            });
        }

        // bind resize event listener
        parent.addEventListener('resize',function(event)
        {
            onResize(local);
        });
      }
    }

    var setLicense = function ( status, key, instance )
    {
        if ( key instanceof Object )
        {
            instance = key;
            key = undefined;
        }

        jQuery.ajax(
        {
            url: Drupal.settings.SiteAttention.urlSaveLicense,
            type: 'POST',
            dataType: 'json',   // response
            contentType: 'application/json', // request
            data: JSON.stringify
            ({
                key: key,
                iid: instance.iid,
                iname: instance.name,
                locked: instance.locked
            }),
            success: function ( data ) {}
        } );
    }

    var saSetHooks = function ()
    {
        SiteAttentionModule.hooks.add('after','register','Saving client data',setLicense);

        SiteAttentionModule.hooks.add('after','instance','Save instance data',setLicense);

        SiteAttentionModule.hooks.add('after','license','Save existing license',setLicense);

        SiteAttentionModule.hooks.add('after','update','UI update',onUpdateUi);

        SiteAttentionModule.hooks.add('after','minimise','Minimise',onMinimise);

        SiteAttentionModule.hooks.add('after','maximise','Maximise',onMaximise);

        SiteAttentionModule.hooks.add('after','play','Starting SiteAttention', onPlay);
    }

    var saInjectScript = function(url,cb,doc)
    {
        if (!instanceOf(url,String)) return;
        if (!instanceOf(doc,HTMLDocument)) doc = document;

        var script = doc.createElement('script');
        script.type = 'text/javascript';
        script.async = true;
        script.src = url;

        if (instanceOf(cb,Function))
            script.addEventListener('load',cb);

        doc.body.appendChild(script);
    }

    var saInjectStyle = function(url,cb,doc)
    {
        if (!instanceOf(url,String)) return;
        if (!instanceOf(doc,HTMLDocument)) doc = document;

        var style = doc.createElement('style');
        style.id = 'SiteAttention_style_css';
        style.type = 'text/css';
        style.media = 'all';
        style.innerText = '@import url("'+url+'");';

        if (instanceOf(cb,Function))
            style.addEventListener('load',cb);

        doc.head.appendChild(style);
    }

    var saLoaded = function ( )
    {
        FieldDrupal7Image = FieldDrupal7Image();

        // Figure out UI environment before SA.play()
        setHtmlEnv();

        // Set the hooks before SA.play()
        saSetHooks();

        SiteAttention.play(
        {
            container: getContainer(),
            minimised: getMinimised()
        } );

        SiteAttention.load(
        {
            cms: SiteAttentionModule.Cms.Drupal,
            pid: getPid(),
            iid: getIid(),
            type: getType(),
            lang: getLang(),
            user: getUserName(),
            url: getUrl(),
            published: getPublished(),
            map: SiteAttentionModule.FieldFactory(getAllFields())
        } );

    }

    saInjectScript( Drupal.settings.SiteAttention.apiUrl, saLoaded , local.document );

};

/**********************End of SiteAttentionDrupal function*****************************/

Drupal.behaviors.siteattention = {
    attach: function ( context, settings )
    {
        if (typeof SiteAttention === 'undefined'){
            window.onload = SiteAttentionDrupal();
        }
    }
}
