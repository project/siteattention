/**
 * @file
 * Script for the SiteAttention settings page.
 */

'use strict';

document.addEventListener('DOMContentLoaded', function(event){

    ! function saInjectSettings(data){
        var id = 'SiteAttentionSettingsScript'
        ,   url = data.apiUrl + 'settings.js?'
        ,   script = document.getElementById(id)
                ? document.getElementById(id)
                : document.createElement('script')
        ,   cb = function (event) {
                document.readyState === 'complete' || document.readyState === 'interactive'
                    ?   init()
                    :   document.addEventListener('DOMContentLoaded', init);
            }
        ,   init = function () {
                new SiteAttentionSettings(data);
            };

        script.id ? cb() : null;

        script.type = 'text/javascript';
        script.src = url;
        script.id = id;

        script.addEventListener('load', cb, false);

        document.body.appendChild(script);
    }
    ( {
        apiUrl: Drupal.settings.SiteAttention.url,
        containerSelector: '#SiteAttentionSettings',
        cms: 'Drupal',
        saVersion: Drupal.settings.SiteAttention.version,
        cmsUrl: Drupal.settings.SiteAttention.cmsUrl,
        cmsHeaders:
        {
            'Content-type': 'application/json'
        },
        settings:
        {
            key: Drupal.settings.SiteAttention.key,
            iid: Drupal.settings.SiteAttention.iid,
            iname: Drupal.settings.SiteAttention.iname,
            locked: Drupal.settings.SiteAttention.ilocked
        }
    } );
});
