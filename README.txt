
-- SUMMARY --

SiteAttention provides support for Search Engine Optimization of your page content before it is being published.In this settings section you can configure how SiteAttention should interact with different properties of your site in Edit mode.

For a full description of the module, visit the project page:
  https://www.drupal.org/project/siteattention
To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/siteattention


-- REQUIREMENTS --

* None.


-- INSTALLATION --

* Install as usual, see
  http://drupal.org/documentation/install/modules-themes/modules-7


-- CONFIGURATION --

* Go to Administration » Configuration » SITEATTENTION SETTINGS » SiteAttention settings,
  and enter the License Key/ Instance Name or ID if not already there.


-- CONTACT --

Current maintainers:
* Morten S. Thorpe - mailto:mt@siteattention.com


This project has been developed by:
* SiteAttention
  The real-time SEO tool for all major content management systems.
  Because your website deserves the best search engine ranking possible
