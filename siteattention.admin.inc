<?php

  // callback: creates the form in plugin settings
  function siteattention_admin_settings() {
    drupal_add_js(
      [
        'SiteAttention' => [
          'url' => SiteAttention::URL,
          'key' => variable_get(SiteAttention::KEY, ''),
          'iid' => variable_get(SiteAttention::IID, ''),
          'iname' => variable_get(SiteAttention::INAME, ''),
          'ilocked' => variable_get(SiteAttention::ILOCKED, FALSE),
          'cmsUrl' => url('siteattention/save/license'),
          'version' => SiteAttention::VERSION,
        ],
      ],
      'setting'
    );

    drupal_add_js(
      drupal_get_path('module', 'siteattention') . '/js/siteattention-admin.js'
    );

    drupal_add_css(
      drupal_get_path(
        'module',
        'siteattention'
      ) . '/css/siteattention-admin.css'
    );

    $form['siteattention/settings'] = [
      '#markup' => '<div id="SiteAttentionSettings"></div>',
    ];

    return $form;
  }
